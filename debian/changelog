ipmiutil (3.1.9-3) unstable; urgency=medium

  * debian/control:
    - Fix Vcs-git URL.

 -- Jörg Frings-Fürst <debian@jff.email>  Sun, 13 Aug 2023 20:06:24 +0200

ipmiutil (3.1.9-2) unstable; urgency=medium

  * New debian/patches/0110-fix_malformed_messages.patch (Closes: #1040349).
  * debian/control:
    - Change Vcs-* to new URL.

 -- Jörg Frings-Fürst <debian@jff.email>  Sun, 09 Jul 2023 14:55:01 +0200

ipmiutil (3.1.9-1) unstable; urgency=medium

  * New upstream release.
    - Refresh patches:
      + debian/patches/0700-init.patch
      + debian/patches/0705-crontab.patch
      + debian/patches/0105-typo.patch
  * Remove old patches:
    - debian/patches/0100-out-of-bounds.patch
    - debian/patches/0710-systemd.patch

 -- Jörg Frings-Fürst <debian@jff.email>  Wed, 15 Mar 2023 21:26:30 +0100

ipmiutil (3.1.8-4) unstable; urgency=medium

  * debian/patches/0705-crontab.patch: Fix typo (Closes: #1030120).
  * debian/control:
    - Remove superseded package lsb-base.
    - Remove version information no longer needed.
  * Declare compliance with Debian Policy 4.6.2.0 (No changes needed).
  * debian/copyright:
    - Add year 2023 to myself.

 -- Jörg Frings-Fürst <debian@jff.email>  Tue, 31 Jan 2023 18:59:52 +0100

ipmiutil (3.1.8-3) unstable; urgency=medium

  * debian/patches/0705-crontab.patch: Fix overwrite binary (Closes: #1022240).

 -- Jörg Frings-Fürst <debian@jff.email>  Mon, 24 Oct 2022 12:39:41 +0200

ipmiutil (3.1.8-2) unstable; urgency=medium

  * debian/patches/0700-init.patch (Closes: #1020558):
    - Fix insert of /lib/lsb/init-functions.
  * Declare compliance with Debian Policy 4.6.1.0 (No changes needed).
  * debian/copyright:
    - Add year 2022 to myself.
  * debian/source/lintian-overrides:
    - Fix typo.

 -- Jörg Frings-Fürst <debian@jff.email>  Sun, 02 Oct 2022 15:01:17 +0200

ipmiutil (3.1.8-1) unstable; urgency=medium

  * New upstream release.
  * Remove useless debian/lintian-overrides.
  * debian/rules:
    - Add autoreconf -i --force.
  * Declare compliance with Debian Policy 4.6.0.1 (No changes needed).
  * debian/copyright:
    - Add 2021 to myself.

 -- Jörg Frings-Fürst <debian@jff.email>  Sat, 27 Nov 2021 15:26:23 +0100

ipmiutil (3.1.7-1) unstable; urgency=medium

  * New upstrewam release.
  * Reinstall init.d scripts.
  * Migrate to debhelper 13:
    - Bump minimum debhelper-compat version in debian/control to = 13.

 -- Jörg Frings-Fürst <debian@jff.email>  Mon, 14 Sep 2020 14:10:17 +0200

ipmiutil (3.1.6-1) unstable; urgency=medium

  * New upstream release.
  * Declare compliance with Debian Policy 4.5.0 (No changes needed).
  * debian/copyright:
    - Add year 2020 for debian/*.

 -- Jörg Frings-Fürst <debian@jff.email>  Sat, 11 Apr 2020 12:07:40 +0200

ipmiutil (3.1.5-1) unstable; urgency=medium

  * New upstream release:
    - Refresh debian/patches/0105-typo.patch.
  * debian/contol:
    - Add anacron to Depends.
  * Switch to debhelper-compat:
    - debian/control: change to debhelper-compat (=12).
    - remove debian/compat.
  * Declare compliance with Debian Policy 4.4.1.2 (No changes needed).
  * debian/control:
    - Add Rules-Requires-Root: no.
  * debian/rules:
    - Remove useless man pages.

 -- Jörg Frings-Fürst <debian@jff.email>  Sun, 22 Dec 2019 20:22:23 +0100

ipmiutil (3.1.4-1) unstable; urgency=medium

  * New upstream release.
  * Migrate to debhelper 12:
    - Change debian/compat to 12.
    - Bump minimum debhelper version in debian/control to >= 12.
  * Declare compliance with Debian Policy 4.4.0 (No changes needed).
  * debian/copyright:
    - Add year 2019 for debian/*.

 -- Jörg Frings-Fürst <debian@jff.email>  Sat, 10 Aug 2019 16:41:37 +0200

ipmiutil (3.1.3-1) unstable; urgency=medium

  * New upstream release.
  * Declare compliance with Debian Policy 4.2.1 (No changes needed).

 -- Jörg Frings-Fürst <debian@jff.email>  Mon, 01 Oct 2018 23:07:55 +0200

ipmiutil (3.1.2-1) unstable; urgency=medium

  * New upstream release.
    - Refresh patches.

 -- Jörg Frings-Fürst <debian@jff.email>  Fri, 03 Aug 2018 18:34:16 +0200

ipmiutil (3.1.1-1) unstable; urgency=high

  * New upstream release.
  * Declare compliance with Debian Policy 4.1.5 (No changes needed).
  * Move source.lintian-overrides to the preferred
      debian/source/lintian-overrides.
  * debian/copyright:
    - Use secure copyright format URI.
    - Change years for Andy Cress.
  * debian/rules:
    - Fix FTBFS with dh_installsystemd (Closes: #901731).
  * debian/control:
    - Switch Vcs-* to new location.
  * Refresh debian/patches/0105-typo.patch.

 -- Jörg Frings-Fürst <debian@jff.email>  Tue, 10 Jul 2018 18:21:47 +0200

ipmiutil (3.0.8-1) unstable; urgency=medium

  * New upstream release.
    - Refresh debian/patch/0700-init.patch.
  * Change to my new mail address:
    - debian/control,
    - debian/copyright
  * Declare compliance with Debian Policy 4.1.3 (No changes needed).
  * Migrate to debhelper 11:
    - Change debian/compat to 11.
    - Bump minimum debhelper version in debian/control to >= 11.
  * debian/rules (Closes: #877211):
    - Add --no-parallel to fix frequent parallel FTBFS.
      Thanks to Adrian Bunk <bunk@debian.org>.
    - Switch from deprecated dh_systemd_enable to dh_installsystemd.

 -- Jörg Frings-Fürst <debian@jff.email>  Tue, 06 Mar 2018 07:43:32 +0100

ipmiutil (3.0.7-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches:
    - debian/patches/0700-init.patch.
    - debian/patches/0705-crontab.patch.
  * New source.lintian-overrides to override ancient-libtool warning.
    - With compat level 10 autoreconf called every time.
  * Add new section at README.source to explain the branching model used.
  * Declare compliance with Debian Policy 4.1.1. (No changes needed).
  * Drop autotools-dev from Build-Depends and drop "--with autotools-dev"
    from dh because this is already default when using dh compatibility
    level 10.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Sat, 14 Oct 2017 19:41:11 +0200

ipmiutil (3.0.1-1) unstable; urgency=medium

  * New upstream release.
  * Fix configuration issues with service files (Closes: #848861):
    - debian/rules:
      + Add override_dh_systemd_enable to install *.service
        files and add --no-enable option to all services which
        need an IPMI driver.
      + Remove override_dh_installinit so no conflicting
        init scripts are installed.
      + Add override_dh_system_start with --no-start and
        --restart-after-upgrade for all daemons which need
        an IPMI driver.
    Thanks to Andreas Henriksson <andreas@fatal.se>!
  * Drop both dh-autoreconf and dh-systemd from both Build-Depends
    and drop "--with systemd --with autoreconf" from dh because
    this is already default when using dh compatibility level 10.
    Thanks to Andreas Henriksson <andreas@fatal.se>!
  * Migrate to OpenSSL 1.1.0 (Closes: #828353)
    - debian/control:
      + Replace libssl1.0-dev with libssl-dev in Build-Depends.
      + Add openssl to Build-Depends (required for configure script).
  * Move purge action from prerm to postrm script to
    properly clean up /var/lib/ipmiutil/ directory.
  * Remove cruft files from /etc/init.d/:
    - New debian/maintscript.
    - debian/control:
      + Add dpkg to Pre-Depends for maintscript.
  * debian/copyright:
    - Update copyright years for 2017.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Tue, 03 Jan 2017 03:38:28 +0100

ipmiutil (3.0.0-2) unstable; urgency=medium

  * debian/control:
    - Switch Build-Depends from libssl-dev to libssl1.0-dev
      until ipmiutil has been ported to OpenSSL 1.1.0.
    - Bump debhelper Build-Depends minimum version to 10.
    - Add missing lsb-base to Depends for binary:ipmiutil
      as suggested by lintian.
  * debian/compat:
    - Bump compat level to 10.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Fri, 18 Nov 2016 13:59:54 +0100

ipmiutil (3.0.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Add missing build dependency libtool-bin.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Wed, 24 Aug 2016 20:50:13 +0200

ipmiutil (2.9.9-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * debian/control:
    - Correct Vcs-Git.
    - Bump Standards-Version to 3.9.8 (no changes required).
  * New debian/patches/0705-crontab.patch (LP: #1535408):
    - Prevent running watchdog to send mails and log entries every minute.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Sun, 15 May 2016 23:06:42 +0200

ipmiutil (2.9.8-1) unstable; urgency=medium

  * New upstream release.
    - Refresh patches.
  * debian/copyright: Add year 2016.
  * debian/control:
    - Bump Standards-Version to 3.9.7 (no changes required).
    - Change Vcs-* to secure uri.
  * debian/watch: Bump Version to 4 (no changes required).
  * New debian/patches/0105-typo.patch.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Thu, 18 Feb 2016 03:19:27 +0100

ipmiutil (2.9.7-1) unstable; urgency=medium

  * New upstream release.
  * Refresh and rename patches.
  * Remove erroneous debian/ipmiutuil.ipmi_port.init.
  * Replace deprecated portmap with rpcbind (Closes: #796364):
    - debian/patches/0700-init.patch:
      + Replace portmap with rpcbind in scripts/ipmi_port.sh.
    - debian/control:
      + Add rpcbind to Depends.
  * debian/rules:
    - Pass "--with autoreconf" to dh.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Thu, 17 Dec 2015 21:42:37 +0100

ipmiutil (2.9.6-1) unstable; urgency=low

  * Initial release (Closes: #753881).

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Fri, 29 May 2015 08:27:28 +0200
